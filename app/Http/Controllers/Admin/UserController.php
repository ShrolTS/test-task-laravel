<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

//use Illuminate\Database\Eloquent\Collection;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application| \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id','!=',auth()->id())
                 ->paginate(5);

        return view('admin.users.index', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    protected function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // 1-й спосіб ВИКОРИСТАННЯ СТАНДАРТНОГО ОБЄКТА Validator
        Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required|min:5',
            'email' => 'required|email|unique:users',
        ],
        )->validate();

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $result = User::create($data);

        if ($result) {
            return redirect()->route('admin.users.index')
                ->with(['success' => 'Успішно збережено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Помилка при зберіганні!'])
                ->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::find($id);
        return view('admin.users.index', compact('users'));
    }


    /**
     * Показує форму для редагування користувача - доступ за id
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public
    function edit(int $id)
    {
        // приклад використання ORM моделі
        $user = User::find($id);
        return view('admin.users.edit', compact('user'));
    }

//    /** АБО так можна:
//     * Show the form for editing the specified resource.
//     * Використання моделі роботи з БД ORM, обєкт User, як параметр
//     * @param  User  $user
//     * @return \Illuminate\Http\Response
//     */
//    public function edit(User $user)
//    {
//        return view('admin.users.edit', compact('user'));
//    }


    /**
     * Оновлення даних користувача за його id
     * ORM
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public
    function update(UserUpdateRequest $request, int $id)
    {   //2-й спосіб валідації використовуємо App\Http\Requests\UserUpdateRequest

        /*закоментовано 3-й спосіб
        $rules=[
             'name' => 'required',
             'password' => 'required|min:5',
             'email' => 'required|email|exists:users,email',
         ];
        $validator=Validator::make($request->all(),$rules)->validate();
        */
        $user = User::find($id);
        if (empty($user)) {
            return back()
                ->withErrors(['msg' => "Користувач з id=[{$id}] не знайдена!"])
                ->withInput();
        }

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $result = $user->fill($data)->save();

        if ($result) {
            return redirect()->route('admin.users.index')
                ->with(['success' => 'Успішно збережено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Помилка при зберіганні!'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * Видалення ORM
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public
    function destroy(User $user)
    {
        $user->delete();
        return redirect()
            ->route('admin.users.index')
            ->with('success', 'Користувач видалений');
    }


}
