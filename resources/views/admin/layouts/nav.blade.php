<ul class="nav">
    <li class="nav-item ">
        <a class="nav-link" href="{{route('admin.index')}}">
            <i class="nc-icon nc-chart-pie-35"></i>
            <p>Info</p>
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link"
           href="{{route('admin.users.index')}}"
        >
            <i class="nc-icon nc-circle-09"></i>
            <p>All Users</p>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('admin.users.create')}}">
            <i class="nc-icon nc-simple-add"></i>
            <p>Add New Users</p>
        </a>
    </li>
    <li>

        <a class="nav-link" href="{{ route('user.logout') }}"
           onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            <i class="nc-icon nc-key-25"></i>
            <p>Log out</p>
        </a>
    </li>
</ul>
