@extends('admin.layouts.app')

@section('content')
    @php
        /**
        * {{-- * @var \App\Models\User $user--}}
        */
    @endphp
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('admin.users.index') }}">Users</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page"> Create user</li>
        </ol>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit User</h4>
                        @if(session('success'))
                            <div class="row justify-content-center">
                                <div class="col-md-11">
                                    <div class="alert alert-danger" role="alert">
                                        {{session()->get('success')}}
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($errors->any())
                            <div class="row justify-content-center">
                                <div class="col-md-11">
                                    <div class="alert alert-danger" role="alert">
                                        @foreach($errors->all() as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">

                        <form id="form" method="POST" action="{{ route('admin.users.store') }}">
                            @csrf
                            @method('POST')
                            <div class="form-group">
                                <label>id (disabled)</label>
                                <input type="text" class="form-control"
                                       name="id"
                                       value=""
                                       disabled placeholder="id">
                            </div>

                            <div class="form-group">
                                <label> Name </label>
                                <input type="text" class="form-control"
                                       name="name" placeholder="UserName"
                                       class="form-control @error('name') is-invalid @enderror"
                                       value="{{old('name')}}">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Email </label>
                                <input type="text" class="form-control"
                                       name="email" placeholder="login@gamil.com"
                                       class="form-control @error('email') is-invalid @enderror"
                                       value="{{old('email')}}">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label> Password </label>
                                <input type="text" class="form-control"
                                       name="password" placeholder="password"
                                       class="form-control @error('password') is-invalid @enderror"
                                       value="{{old('password')}}">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <button type="submit" name="save_user" class="btn btn-primary btn-fill pull-right">Save
                            </button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection

