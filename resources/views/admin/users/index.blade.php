@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ asset('admin.index') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Users
                            </li>

                        </ol>
                    </nav>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card strpied-tabled-with-hover">
                                    <div class="card-header ">
                                        <h4 class="card-title">Users </h4>
                                        <p class="card-category">

                                        </p>
                                    </div>
                                    <div class="card-body table-full-width table-responsive">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Password</th>
                                            <th>Options</th>
                                            </thead>
                                            <tbody>
                                            @foreach($users  as $user)
                                                 @php /** @var App\Models\User $user */ @endphp
{{--                                                @if ($user->id == auth()->id())--}}
{{--                                                    @continue--}}
{{--                                                @endif--}}
                                                <tr>
                                                    <td>{{$user->id}}</td>
                                                    <td>
                                                        <a href="#">
                                                            {{$user->name}}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        {{$user->email}}
                                                    </td>
                                                    <td>
                                                        {{$user->password}}
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('admin.users.edit', ['user' => $user->id]) }}">
                                                            {{--                                                            <i class="glyphicon glyphicon-edit"></i>--}}
                                                            <img
                                                                src="{{ asset('assetsAdmin/img/button/user-edit-icon.png')}}">

                                                        </a>
                                                        <form action="{{ route('admin.users.destroy', $user) }}"
                                                              method="post"
                                                              onsubmit="return confirm('Видалити цього користувача?')">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit"
                                                                    class="m-0 p-0 border-0 bg-transparent">
                                                                <img
                                                                    src="{{ asset('assetsAdmin/img/button/user-delete-icon.png')}}">

                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    @if($users->total()>=$users->count())
                        <br>
                        <div class="row justify-content-center">
                            {{$users->links("pagination::bootstrap-4")}}
                        </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
@endsection
