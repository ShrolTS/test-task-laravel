<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\IndexController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::name('user.')->prefix('user')->group(function () {
    Auth::routes();
});

Route::name('admin.')
    ->prefix('admin')
    ->namespace('Admin')
    ->middleware('auth')
    ->group(function () {
        // головна сторінка панелі керування
        Route::get('', [IndexController::class, 'index'])->name('index');
        // CRUD-операції над користувачами: admin.users.index,admin.users.edit, admin.users.create і т.д.
        Route::resource("users", 'UserController');
    });

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
